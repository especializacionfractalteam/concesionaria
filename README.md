# README #

## Diego Izquierdo  20201099035 ##
## Mateo Puerto     20201099044 ##

Modelado de plataforma web de concesionaria de vehículos de alta gama, usando UML, la plataforma debe permitir al usuario cambiar la configuración de las partes del vehículo de acuerdo a las posibles existencias para el modelo seleccionado y probar el vehículo en una simulación que informe al usuario sobre:

* el consumo de combustible.
* la aceleración del vehículo.
* la potencia del motor.

### Diagrama de casos de uso ###
![Alt text](imgs/Casos_de_uso.png)

### Diagrama de clases ###
![Alt text](imgs/Diagrama_de_clases.png)

### Diagrama de componentes ###
![Alt text](imgs/Diagrama_de_componentes.png)	
	
### Diagrama de objetos ###
![Alt text](imgs/Diagrama_de_objetos.png)

### Diagrama de paquetes ###
![Alt text](imgs/Diagrama_de_paquetes.png)

### Diagrama de despliegue ###
![Alt text](imgs/Diagrama_de_despliegue.png)


### Diagrama de actividades ###
![Alt text](imgs/Diagrama_actividades.png)

### Diagrama de secuencia ###
![Alt text](imgs/Diagrama_secuencia.png)

### Diagrama de colaboración ###
![Alt text](imgs/Diagrama_colaboracion.png)

### Diagrama de maquina de estados ###
![Alt text](imgs/Diagrama_maquina_estados.png)

